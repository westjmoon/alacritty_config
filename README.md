### Alacritty 配置文件

- 背景透明: 0.8
- 字体：`UbuntuMono Nerd Font Mono` 
- 主题：`One Dark`,主题来源：Github [alacritty-themes](https://github.com/eendroroy/alacritty-theme) 
